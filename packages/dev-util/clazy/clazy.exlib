# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/${PV}/src" ] cmake

export_exlib_phases src_test

SUMMARY="Qt oriented code checker based on clang framework"

HOMEPAGE="https://www.kdab.com/use-static-analysis-improve-performance/"

LICENCES="LGPL-2"
SLOT="0"
MYOPTIONS=""

CLANG_MIN_VER="8.0"

DEPENDENCIES="
    build:
        dev-lang/perl:* [[ note = [ pod2man ] ]]
    build+run:
        dev-lang/clang:*[>=${CLANG_MIN_VER}]
        dev-lang/llvm:=[>=${CLANG_MIN_VER}]
"

# Many tests fails with something like
# "cstddef:50:10: fatal error: 'stddef.h' file not found"
# I suppose this is due to our unusual (system) include dirs.
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DCLAZY_ENABLE_SANITIZERS:BOOL=FALSE
    -DCLAZY_MAN_PAGE:BOOL=TRUE
)

clazy_src_test() {
    # run_tests.py wants to run clang --version, which is banned by us due to
    # being unprefixed
    export CLANGXX=$(exhost --target)-clang

    cmake_src_test
}

