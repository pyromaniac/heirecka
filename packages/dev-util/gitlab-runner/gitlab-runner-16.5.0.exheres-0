# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="An application that works with GitLab CI/CD to run jobs in a pipeline"

HOMEPAGE="https://gitlab.com/gitlab-org/gitlab-runner"
DOWNLOADS="https://gitlab.com/gitlab-org/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

UPSTREAM_DOCUMENTATION="https://docs.gitlab.com/runner/"

DEPENDENCIES="
    build:
        dev-golang/gox
        dev-lang/go
    build+run:
        group/gitlab-runner
        user/gitlab-runner
"

WORK="${WORKBASE}"/${PN}-v${PV}

_COMMIT=d89a789a

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    config.toml.example
)

src_unpack() {
    default
    export GOMODCACHE="${FETCHEDDIR}/go/pkg/mod"

    esandbox disable_net

    edo pushd "${WORK}"
    edo go mod download -x
    edo popd

    esandbox enable_net
}

src_prepare() {
    default

    # Pass EXJOBS through to gox
    edo sed -e "s/-output=\"/-parallel=${EXJOBS:-1} -output=\"/" \
        -i Makefile.build.mk
}

src_compile() {
    export CGO_LDFLAGS="${LDFLAGS}"

    emake \
        GOX=/usr/$(exhost --build)/bin/gox \
        REVISION=${_COMMIT} \
        VERSION=${PV} runner-bin-host
}

src_test() {
    CI=0 edo go test
}

src_install() {
    dobin out/binaries/${PN}

    install_systemd_files

    keepdir /etc/${PN} /var/lib/${PN}
    edo chown gitlab-runner:gitlab-runner \
        "${IMAGE}"/etc/${PN} \
        "${IMAGE}"/var/lib/${PN}
    edo chmod 0700 "${IMAGE}"/etc/${PN} "${IMAGE}"/var/lib/${PN}

    emagicdocs
}

