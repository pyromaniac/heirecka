# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="v${PV}"

require github [ user=KDAB release=v${PV} suffix=tar.gz ] cmake
require gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="The Linux perf GUI for performance analysis"
DESCRIPTION="
This project is a KDAB R&D effort to create a standalone GUI for performance
data. As the first goal, we want to provide a UI like KCachegrind around Linux
perf. Looking ahead, we intend to support various other performance data
formats under this umbrella."

HOMEPAGE+=" https://www.kdab.com/"

# There are also licences for commercial usage, see LICENSE{,.US}.txt
LICENCES="GPL-2.0"
SLOT="0"
MYOPTIONS="
    zstd [[ description = [ Transparent support for reading zstd compressed perf data files ] ]]
"

QT_MIN_VER="5.10"

DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=1.0.0]
    build+run:
        dev-util/elfutils
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kio:5
        kde-frameworks/kitemmodels:5
        kde-frameworks/kitemviews:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/solid:5
        kde-frameworks/threadweaver:5
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        zstd? ( app-arch/zstd )
    run:
        sys-analyzer/perf
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DAPPIMAGE_BUILD:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( Zstd )

hotspot_src_prepare() {
    cmake_src_prepare

    # Disable a test which wants to run perf, mount kernel modules, etc
    edo sed -e "/^add_subdirectory(integrationtests)/d" -i tests/CMakeLists.txt
}

