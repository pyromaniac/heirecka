# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=3MFConsortium tag=v${PV} ] cmake

export_exlib_phases src_prepare src_install

SUMMARY="A C++ implementation of the 3D Manufacturing Format file standard"
DESCRIPTION="
It provides 3MF reading and writing capabilities, as well as conversion and
validation tools for input and output data. lib3mf offers a clean and
easy-to-use API in various programming languages to speed up the development
and keep integration costs at a minimum.
As 3MF shall become an universal 3D Printing standard, its quick adoption is
very important. This library shall lower all barriers of adoption to any
possible user, let it be software providers, hardware providers, service
providers or middleware tools."

HOMEPAGE+=" https://3mf.io/"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-golang/automaticcomponenttoolkit
        virtual/pkg-config
    build+run:
        app-arch/libzip
        sys-libs/zlib
    test:
        dev-cpp/gtest
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

UPSTREAM_DOCUMENTATION="https://lib3mf.readthedocs.io/en/master/"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_INCLUDEDIR="include/${PN}"
    -DBUILD_FOR_CODECOVERAGE:BOOL=FALSE
    -DUSE_INCLUDED_GTEST:BOOL=FALSE
    -DUSE_INCLUDED_LIBZIP:BOOL=FALSE
    -DUSE_INCLUDED_SSL:BOOL=FALSE
    -DUSE_INCLUDED_ZLIB:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DLIB3MF_TESTS:BOOL=TRUE -DLIB3MF_TESTS:BOOL=FALSE'
)

lib3mf_src_prepare() {
    cmake_src_prepare

    # It uses a binary bundled with the source, which is not something we want
    # in general and it would not work on other arches, eg. arm.
    # https://github.com/3MFConsortium/lib3mf/issues/199
    edo rm AutomaticComponentToolkit/bin/act.linux
    sed -e \
        's|${CMAKE_CURRENT_SOURCE_DIR}/AutomaticComponentToolkit/bin/act.${ACT_COMMANDENDING}|act|' \
        -i CMakeLists.txt
}

lib3mf_src_install() {
    cmake_src_install

    local includedir=/usr/$(exhost --target)/include/${PN}
    for suffix in abi types implicit; do
        dosym ${includedir}/Bindings/Cpp/${PN}_${suffix}.hpp ${includedir}/${PN}_${suffix}.hpp
    done
}

