# Copyright 2014-2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ] cmake
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare src_compile pkg_postinst pkg_postrm

SUMMARY="A fast Qt IMAP e-mail client"
DESCRIPTION="It aims to deliver an application which:
* Enables you to access your mail anytime, anywhere.
* Does not slow you down. If we can improve the productivity of an e-mail
  user, we better do.
* Respects open standards and facilitate modern technologies. We value the
  vendor-neutrality that IMAP provides and are committed to be as
  interoperable as possible.
* Is efficient -- be it at conserving the network bandwidth, keeping memory
  use at a reasonable level or not hogging the system's CPU.
* Can be used on many platforms. One UI is not enough for everyone, but our
  IMAP core works fine on anything from desktop computers to cell phones and
  big ERP systems.
* Plays well with the rest of the ecosystem. We don't like reinventing wheels,
  but when the existing wheels quite don't fit the tracks, we're not afraid of
  making them work."

HOMEPAGE+=" http://trojita.flaska.net/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS=""

if ever at_least scm ; then
    MYOPTIONS="
        akonadi [[ description = [ Build a plugin to store contacts with akonadi ] ]]
        spell   [[ description = [ Build a plugin for spellchecking via KDE's Sonnet ] ]]
    "
    QT_MIN_VER="5.15.0"
else
    QT_MIN_VER="5.2.0"
fi

DEPENDENCIES="
    build:
        dev-util/ragel
        virtual/pkg-config
    build+run:
        app-crypt/gnupg
        mail-libs/mimetic
        sys-auth/qtkeychain[providers:qt5]
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
        x11-libs/qtwebkit:5[>=${QT_MIN_VER}]
"

if ever at_least scm ; then
    DEPENDENCIES+="
        build+run:
            app-crypt/gpgme[>=1.8.0]
            app-crypt/libqgpgme[>=1.8.0][providers:qt5]
            akonadi? ( kde-frameworks/akonadi-contact:5 )
            spell? ( kde-frameworks/sonnet:5 )
    "
    CMAKE_SRC_CONFIGURE_TESTS+=(
        '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    )
else
    DEPENDENCIES+="
        build+run:
            app-crypt/gpgme[>=1.7.0]
            app-crypt/libqgpgme[>=1.7.0][providers:qt5]
    "
    CMAKE_SRC_CONFIGURE_TESTS+=(
        '-DWITH_TESTS:BOOL=TRUE -DWITH_TESTS:BOOL=FALSE'
    )
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DWITH_DBUS:BOOL=TRUE
    -DWITH_DESKTOP:BOOL=TRUE
    -DWITH_GPGMEPP:BOOL=TRUE
    -DWITH_KF5_GPGMEPP:BOOL=TRUE
    -DWITH_MIMETIC:BOOL=TRUE
    -DWITH_QTKEYCHAIN_PLUGIN:BOOL=TRUE
    -DWITH_SHARED_PLUGINS:BOOL=TRUE
    # otherwise the bundled ragel is used
    -DWITH_RAGEL:BOOL=TRUE
    # Support for COMPRESS=DEFLATE enabled
    -DWITH_ZLIB:BOOL=TRUE
)

if ever is_scm ; then
    CMAKE_SRC_CONFIGURE_OPTION_WITHS+=(
        'akonadi AKONADIADDRESSBOOK_PLUGIN'
        'spell SONNET_PLUGIN'
    )

fi

DEFAULT_SRC_INSTALL_EXCLUDE+=( README.debian )

trojita_src_prepare() {
    cmake_src_prepare

    # These tests need X
    edo sed -e '/trojita_test(Composer Html_formatting)/d' \
            -e '/target_link_libraries(test_Html_formatting/d' \
            -e '/qt5_use_modules(test_Html_formatting WebKitWidgets)/d' \
            -i CMakeLists.txt
}

trojita_src_compile() {
    esandbox allow_net "unix:${WORK}/keys/S.gpg-agent"
    esandbox allow_net --connect "unix:${WORK}/keys/S.gpg-agent"
    esandbox allow_net "unix:${WORK}/keys/S.gpg-agent.browser"
    esandbox allow_net --connect "unix:${WORK}/keys/S.gpg-agent.browser"
    esandbox allow_net "unix:${WORK}/keys/S.gpg-agent.extra"
    esandbox allow_net --connect "unix:${WORK}/keys/S.gpg-agent.extra"
    esandbox allow_net "unix:${WORK}/keys/S.gpg-agent.ssh"
    esandbox allow_net --connect "unix:${WORK}/keys/S.gpg-agent.ssh"

    cmake_src_compile

    esandbox disallow_net "unix:${WORK}/keys/S.gpg-agent.ssh"
    esandbox disallow_net --connect "unix:${WORK}/keys/S.gpg-agent.ssh"
    esandbox disallow_net "unix:${WORK}/keys/S.gpg-agent.extra"
    esandbox disallow_net --connect "unix:${WORK}/keys/S.gpg-agent.extra"
    esandbox disallow_net "unix:${WORK}/keys/S.gpg-agent.browser"
    esandbox disallow_net --connect "unix:${WORK}/keys/S.gpg-agent.browser"
    esandbox disallow_net "unix:${WORK}/keys/S.gpg-agent"
    esandbox disallow_net --connect "unix:${WORK}/keys/S.gpg-agent"
}


trojita_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

trojita_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

