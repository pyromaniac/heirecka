# Copyright 2015, 2020-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_SECONDARY_REPOSITORIES="gaia"
SCM_EXTERNAL_REFS="
    src/tools/celestia-gaia-stardb:gaia
    thirdparty/fmt:
    thirdparty/miniaudio:
    thirdparty/Spice:
"
SCM_content_REPOSITORY="https://github.com/CelestiaProject/CelestiaContent.git"
SCM_gaia_REPOSITORY="https://github.com/ajtribick/celestia-gaia-stardb.git"

require github [ user=CelestiaProject ] cmake
require ffmpeg [ with_opt=true option_name=video-capture ]

SUMMARY="A 3D space simulation that lets you travel through the solar system"

DESCRIPTION="
Unlike most planetarium software, Celestia doesn't confine you to the surface
of the Earth. You can travel throughout the solar system, to any of over
100,000 stars, or even beyond the galaxy.
All movement in Celestia is seamless; the exponential zoom feature lets you
explore space across a huge range of scales, from galaxy clusters down to
spacecraft only a few meters across. A 'point-and-goto' interface makes it
simple to navigate through the universe to the object you want to visit.
Celestia is expandable. Celestia comes with a large catalog of stars,
galaxies, planets, moons, asteroids, comets, and spacecraft. If that's not
enough, you can download dozens of easy to install add-ons with more objects."

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    avif    [[ description = [ Support for the AV1 Image File Format ] ]]
    gtk3
    qt5
    qt6     [[ description = [  Adds support for the Qt GUI/Application Toolkit version 6.x ] ]]
    video-capture [[ description = [ Support video capture using FFMPEG ] ]]
    wayland [[ description = [ Use Wayland in the Qt frontend ] ]]

    wayland? ( ( qt5 qt6 ) [[ number-selected = at-least-one ]] )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( gtk3 qt5 qt6 ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        wayland? ( sys-libs/wayland-protocols )
    build+run:
        dev-lang/LuaJIT
        dev-libs/fmt[>=6.1.0]
        dev-libs/icu:=
        dev-libs/libepoxy
        dev-libs/meshoptimizer
        media-libs/freetype:2
        media-libs/libpng:=
        sci-libs/eigen:3[>=3.3]
        x11-dri/glu
        x11-dri/mesa
        avif? ( media-libs/libavif:= )
        gtk3? (
            dev-libs/glib:2
            x11-libs/gtk+:3
            x11-libs/cairo
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        qt5? ( x11-libs/qtbase:5 )
        qt6? (
            x11-libs/qt5compat:6
            x11-libs/qtbase:6
        )
        wayland? ( sys-libs/wayland )
    run:
        sci-astronomy/celestia-content
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DENABLE_CELX:BOOL=TRUE
    -DENABLE_MINIAUDIO:BOOL=FALSE
    # If there's interest this could be an option
    -DENABLE_SDL:BOOL=FALSE
    -DENABLE_SPICE:BOOL=FALSE
    -DUSE_ICU:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    'avif LIBAVIF'
    'gtk3 GTK'
    'video-capture FFMPEG'
    QT5
    QT6
)

CMAKE_SRC_CONFIGURE_OPTION_USES+=(
    GTK3
    WAYLAND
)

