# Copyright 2015 Volodymyr Medvid <vmedvid@riseup.net>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_prepare

SUMMARY="Terminal keypress reading library"
DESCRIPTION="
libtermkey allows easy processing of keyboard entry from terminal-based programs.
It handles all the necessary logic to recognise special keys, UTF-8 combining,
and so on, with a simple interface.
"
HOMEPAGE="https://www.leonerd.org.uk/code/libtermkey"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/unibilium:=[>=0.1.0]
"

common_params=(
    PREFIX=/usr/$(exhost --target)
    MANDIR=/usr/share/man
)

DEFAULT_SRC_COMPILE_PARAMS=( "${common_params[@]}" )
DEFAULT_SRC_INSTALL_PARAMS=( "${common_params[@]}" )

libtermkey_src_prepare() {
    default

    edo sed -i 's/pkg-config/$(PKG_CONFIG)/g' Makefile

    # glib:2 is only used for a demo, which isn't installed anyway
    edo sed -e 's/pkgconfig, glib-2.0/pkgconfig, disable-glib/' -i Makefile
}

