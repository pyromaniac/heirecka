Title: `dev-util/bats' is renamed to `dev-util/bats-core'
Author: Edward Bates <gitmux@stemux.com>
Content-Type: text/plain
Posted: August-23-2022
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-util/bats

Please install `bats-core' and *afterwards* uninstall `bats'.

1. Take note of any packages depending on `dev-util/bats':
cave resolve \!dev-util/bats

2. Install <new package name>:
cave resolve dev-util/bats-core -x

3. Re-install the packages from step 1.

4. Uninstall `dev-util/bats'
cave resolve \!dev-utils/bats

Do it in *this* order or you'll potentially *break* your system.
